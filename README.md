# flask-ci

## Project Set Up
```
$python3 -m venv env
$pip install --upgrade pip
$pip install -r requirements.txt
```

## Run with Docker
```
$docker build -t hello-flask .
$docker run --rm -d -p 8080:8080 hello-flask:latest
```
