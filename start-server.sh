#!/bin/sh
source env/bin/activate
exec gunicorn -b 0.0.0.0:8080 --access-logfile - --error-logfile - hello:app