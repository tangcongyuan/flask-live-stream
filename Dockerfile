FROM python:3.6-alpine

ENV FLASK_APP hello.py

RUN ln -s /bin/sh /bin/bash

RUN adduser -D cytang
USER cytang
RUN mkdir -p /home/cytang/flask

WORKDIR /home/cytang/flask

COPY requirements.txt requirements.txt
RUN python -m venv env
RUN env/bin/pip install --upgrade pip
RUN env/bin/pip install -r requirements.txt

COPY hello.py start-server.sh ./

# runtime configuration
ENTRYPOINT [ "./start-server.sh" ]
